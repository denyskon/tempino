#!/usr/bin/python3
## Code by DK (lxdb)
#QT-Bibliotheken importieren
from PyQt5 import *
from PyQt5.QtCore import *
from PyQt5.QtWidgets import *
from PyQt5.QtGui import *
#andere Bibliotheken importieren
import sys
import sqlite3
import serial
import time
import os
#Qt-Anwendung erstellen
app = QApplication(sys.argv)

#Variablen definieren
id=0
ende=0
icon1 = QIcon('/opt/tempino/icons/send.svg')
icon2 = QIcon('/opt/tempino/icons/edit.svg')
icon3 = QIcon('/opt/tempino/icons/about.svg')
icon4 = QIcon('/opt/tempino/icons/license.svg')
#Objekt
class ldb_window(QWidget):
    def __init__(self):
        super().__init__()
        self.initMe()
    def initMe(self):
        self.bar = QMenuBar()
        board = self.bar.addMenu('&Board')
        help = self.bar.addMenu('&Hilfe')
        edit = QAction(QIcon(icon2),'&Arduino-Programm bearbeiten',self)
        edit.triggered.connect(self.editact)
        upload = QAction(QIcon(icon1),'&Arduino-Programm hochladen',self)
        upload.triggered.connect(self.upload)
        board.addAction(upload)
        board.addAction(edit)
       
        liact = QAction(QIcon(icon3),'&Lizenz',self)
        liact.triggered.connect(self.licacti)
        liact.setToolTip('Lizenz anzeigen')
        help.addAction(liact)
        edit1 = QAction(QIcon(icon2),'&Upload-Skript bearbeiten',self)
        edit1.triggered.connect(self.editact1)
        about = QAction(QIcon(icon3),'&Ueber',self)
        about.triggered.connect(aboutwin.show)
        about.setToolTip('Über das Programm')
        help.addAction(about)
        board.addAction(edit1)
     #--Block1--
        
        self.U1 = QLabel('Temperaturdatenbank anlegen')
        self.U1.setFont(QFont('Arial Bold',14))
        self.db_Label1 = QLabel('Datenbankname')
        self.db_Entry1 = QLineEdit()
        self.dbanB = QPushButton('Datenbank anlegen')
        self.dbanB.clicked.connect(self.db_anlegen)
        #Trenner
        self.str = QLabel('---------------------------------')
        self.str.setFont(QFont('Noto Sans' , 20))
        
        #--Block2--
        self.U2 = QLabel('Daten einschreiben')
        self.U2.setFont(QFont('Arial Bold',14))
        self.db_Label2 = QLabel('Datenbankname')
        self.db_Entry2 = QLineEdit()
        self.portL = QLabel('Serielle Schnittstelle')
        self.portE = QLineEdit()
        self.db_Label_id = QLabel('ID')
        self.db_Entry_id = QLineEdit()
        self.dbschrB = QPushButton('Messung Beginnen')
        self.dbschrB.setToolTip('Es werden Messdaten von einem Arduino-Board\nausgelesen und in eine Datenbank geschrieben.')
            
        self.dbschrB.clicked.connect(self.db_schreiben)
        
        self.box = QVBoxLayout()
        self.box.addWidget(self.bar)
        self.box.addWidget(self.U1)
        self.box.addWidget(self.db_Label1)
        self.box.addWidget(self.db_Entry1)
        self.box.addWidget(self.dbanB)
        self.box.addWidget(self.str)
        self.box.addWidget(self.U2)
        self.box.addWidget(self.db_Label2)
        self.box.addWidget(self.db_Entry2)
        self.box.addWidget(self.portL)
        self.box.addWidget(self.portE)
        self.box.addWidget(self.db_Label_id)
        self.box.addWidget(self.db_Entry_id)
        self.box.addWidget(self.dbschrB)
        self.setLayout(self.box)

        self.setWindowTitle("TempIno v1.0(Qt)")
        self.setWindowIcon(QIcon("/opt/tempino/LinuxDB.png"))
        self.show()







    def db_anlegen(self):
        print("Datenbank angelegt")
        db_name=self.db_Entry1.text()
        connection = sqlite3.connect(db_name)
        cursor= connection.cursor()
        cursor.execute('''
        CREATE TABLE messwert(id INTEGER PRIMARY KEY, Jahr TEXT, Monat TEXT, Tag TEXT, Stunde TEXT, Minute TEXT, Sekunde TEXT, Messwert TEXT)
        ''')


        connection.close()

# Wigdets


    def db_schreiben(self):
        port=self.portE.text()
        s = serial.Serial(port, 9600)
        s.isOpen() #s.open()
        time.sleep(5) # der Arduino resettet nach einer Seriellen Verbindung, daher muss kurz gewartet werden
        global ende
        ende=0
        global id
        db_name3=self.db_Entry2.text()
        connection = sqlite3.connect(db_name3)
        cursor= connection.cursor()
        sql = "SELECT * FROM messwert"
        cursor.execute(sql)
        for row in cursor:
            id =  row[0]

        try:
            id=int(row[0])
            print("id_try",id)
        except:
            id=int(self.db_Entry_id.text())
            print("id_ex",id)

        startzeit=time.time() #Anzahl der Sekunden seit der Epoche in UTC
        connection.close()
        while True:
            db_name2=self.db_Entry2.text()
            connection = sqlite3.connect(db_name2)
            cursor= connection.cursor()
            st_b = s.readline()
            st=st_b.decode("utf-8")# bytearry to String
            st_list=st.split("!")

            stelle_1=st_list[0]
            #print("Erste Stelle ",stelle_1)
            stelle_2=st_list[1]
            #print("Zweite Stelle ",stelle_2)

            print("in Datenbank schreiben")

           
            lt =time.localtime()
            jahr,monat,tag = lt[0:3]
            stunde, minute, sekunde =lt[3:6]
            jahr=str(jahr)



            id=id+1
            print("id ",id)
            sql_text=""
            sql_text += "INSERT INTO messwert VALUES ("
            sql_text += str(id) +","
            sql_text +=" '"+str(jahr) +"',"
            sql_text +=" '"+str(monat) +"',"
            sql_text +=" '"+str(tag) +"',"
            sql_text +=" '"+str(stunde) +"',"
            sql_text +=" '"+str(minute) +"',"
            sql_text +=" '"+str(sekunde) +"',"
            sql_text +=" '"+str(stelle_2) +"')"

            print("sql_text ", sql_text)
            cursor.execute(sql_text)

            connection.commit()
            jetzt_zeit=time.time()
            dif_zeit=jetzt_zeit-startzeit

            connection.close()
            if (ende==1):
                break
    def db_schreiben1():
        port=port_Entry_id.get()
        s = serial.Serial(port, 9600)
        s.isOpen() #s.open()
        time.sleep(5) # der Arduino resettet nach einer Seriellen Verbindung, daher muss kurz gewartet werden
        global ende
        ende=0
        global id
        db_name3=db_Entry2.get()
        connection = sqlite3.connect(db_name3)
        cursor= connection.cursor()
        sql = "SELECT * FROM messwert"
        cursor.execute(sql)
        for row in cursor:
            id =  row[0]

        try:
            id=int(row[0])
            print("id_try",id)
        except:
            id=int(db_Entry_id.get())
            print("id_ex",id)

        startzeit=time.time() #Anzahl der Sekunden seit der Epoche in UTC
        connection.close()
        while True:
            db_name2=db_Entry2.get()
            connection = sqlite3.connect(db_name2)
            cursor= connection.cursor()
            st_b = s.readline()
            st=st_b.decode("utf-8")# bytearry to String
            st_list=st.split("!")

            stelle_1=st_list[0]
            #print("Erste Stelle ",stelle_1)
            stelle_2=st_list[1]
            #print("Zweite Stelle ",stelle_2)

            print("in Datenbank schreiben")

        
            lt =time.localtime()
            jahr,monat,tag = lt[0:3]
            stunde, minute, sekunde =lt[3:6]
            jahr=str(jahr)



            id=id+1
            print("id ",id)
            sql_text=""
            sql_text += "INSERT INTO messwert VALUES ("
            sql_text += str(id) +","
            sql_text +=" '"+str(jahr) +"',"
            sql_text +=" '"+str(monat) +"',"
            sql_text +=" '"+str(tag) +"',"
            sql_text +=" '"+str(stunde) +"',"
            sql_text +=" '"+str(minute) +"',"
            sql_text +=" '"+str(sekunde) +"',"
            sql_text +=" '"+str(stelle_2) +"')"

            print("sql_text ", sql_text)
            cursor.execute(sql_text)

            connection.commit()
            jetzt_zeit=time.time()
            dif_zeit=jetzt_zeit-startzeit
            ende=1
            connection.close()
            if (ende==1):
                break
    def upload(self):
        os.system('pkexec cp -r /opt/tempino/SimpleDHT /usr/share/arduino/libraries/SimpleDHT && pkexec make -C /opt/tempino/files upload')

    def editact(self):
        os.system('python3 /opt/tempino/edit-prg.py')
    def licacti(self):
        liw.show()
    def editact1(self):
        os.system('python3 /opt/tempino/edit-prg1.py')



#Lizenzfenster
box1 = QVBoxLayout()
liL = QLabel('Lizenz')
def litextset():
    litext.clear()
    litext.setText("TempIno v1.0 QT\nCopyright (C) 2019  Denys Konovalov\nThis program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation; either version 3 of the License, or (at your option) any later version.This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warrantyof MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.You should have received a copy of the GNU General Public License along with this program; if not, see <http://www.gnu.org/licenses/>.\n--------------------------------------------------------------------------------------------------------------------------------------------\n\nLinuxDBv2.0 QT\nCopyright (C) 2019 Denys Konovalov \nDieses Programm ist freie Software. Sie können es unter den Bedingungen der GNU General Public License,wie von der Free Software Foundation veröffentlicht, weitergeben und/oder modifizieren, entweder gemäß Version 3 der Lizenz oder (nach Ihrer Option) jeder späteren Version.Die Veröffentlichung dieses Programms erfolgt in der Hoffnung, daß es Ihnen von Nutzen sein wird,aber OHNE IRGENDEINE GARANTIE, sogar ohne die implizite Garantie der MARKTREIFE oder der VERWENDBARKEIT FÜR EINEN BESTIMMTEN ZWECK. Details finden Sie in der GNU General Public License.Sie sollten ein Exemplar der GNU General Public License zusammen mit diesem Programm erhalten haben. Falls nicht, siehe http://www.gnu.org/licenses/.\n---------------------------------------------------------------------------------------------------------------------------------------------")


litext = QTextEdit()
box1.addWidget(liL)
box1.addWidget(litext)
liw = QWidget()
liw.setLayout(box1)

litextset()
liw.setWindowTitle("TempIno v1.0 license")
liw.setWindowIcon(QIcon(icon4))


#Ueber
bild = QPixmap('/opt/tempino/LinuxDB.png')
bcs = bild.scaled(128,128)
box2 = QVBoxLayout()
aboutwin = QWidget()
ablogo = QLabel()
ab1 = QLabel('TempIno v1.0')
ab2 = QLabel('Copyright Denys Konovalov (lxdb)')
ab3 = QLabel('Editor by Martin Fitzpatrick \n(15-minute-apps,No2Pads)')
ab4 = QLabel('''<a href='https://lxdbdatabase.wordpress.com'>lxdb website</a>''')
ablogo.setPixmap(bcs)

ab4.setOpenExternalLinks(True)
box2.addWidget(ablogo)
box2.addWidget(ab1)
box2.addWidget(ab2)
box2.addWidget(ab3)
box2.addWidget(ab4)
aboutwin.setLayout(box2)
aboutwin.setWindowIcon(icon4)
w=ldb_window()
sys.exit(app.exec_())
